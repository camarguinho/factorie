package auto.estudo;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class ConnectionFactory {
	public Connection getConnection() {
        try {
        	String dataBase = System.getenv("typeDataBase");
        	
            Connection conexao = 
                    DriverManager.getConnection("jdbc:"+dataBase+"://localhost:3306/autoEstudo", "pararalatrum1548", "******");

            return conexao;
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }
}
